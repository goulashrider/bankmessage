package bankMessageSystem;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.JUnit4TestAdapter;

public class TestTweet {

	@Test
	public void test() {
		Tweet test = new Tweet();
		test.setMessageBody("Hi, there is something for @you for #holiday @me");
		test.setMessageSender("@me");
		test.getHashtags();
		test.getMentions();
		
		assertEquals(1, test.hashtags.size());
		assertEquals(2, test.mentions.size());
	}

}
