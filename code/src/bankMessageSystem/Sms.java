package bankMessageSystem;



import java.util.*;
import java.util.Map.Entry;

/**
 * 
 */
public class Sms extends Message {

    /**
     * Default constructor
     */
    public Sms() {
    	this.setMessageSubject(null);
    	Abbreviations.getInstance();
    }
    
    public void init() {
        this.abbrevationReplace();
    }
    
    public void abbrevationReplace() {
    	// load message body into an array
    	// split array by space
    	
    	String bodyArray[] = this.messageBody.split(" ");
    	
    	// go through on array and look for abbreviation
    	
    	for (int i=0; i < bodyArray.length; i++) {
    		for(Entry<String, String> entry : Abbreviations.abbreviations.entrySet()){ 
    			
    			// abbrevation checking lower and uppercase as well
    			
    			if ( entry.getKey().toString().equals(bodyArray[i]) || entry.getKey().toString().toLowerCase().equals(bodyArray[i]) ) {
    				
    				// expand text with abbreviation explanation in closing <> 
    				
    				String abbrevExplain = " <" + entry.getValue().toString() + ">";
    				bodyArray[i] = bodyArray[i] + abbrevExplain;
    			}
    		}
    	}
    	
    	// convert body array back to string
    	String joined = String.join(" ", bodyArray);
    	this.setMessageBody(joined);
    	
    	// test
    	//System.out.println(this.getMessageBody());
    }

}