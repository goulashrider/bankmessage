package bankMessageSystem;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

public class Abbreviations {
	
	protected static Abbreviations instance;
	public static HashMap<String, String> abbreviations;
	

	private Abbreviations() throws ParseException, IOException {
		abbreviations = new HashMap<>();
		loadData();
	}
	
	public static synchronized Abbreviations getInstance() {
		if (instance == null) {
			try {
				instance = new Abbreviations();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;
	}
	
	public static void loadData() throws ParseException, IOException {
		
		BufferedReader br = new BufferedReader(new FileReader("data/textwords.csv"));
	    String line =  null;
	    
	    while((line=br.readLine())!=null){
	        String str[] = line.split(",");
		    abbreviations.put(str[0], str[1]);
	        
	    }
	    //System.out.println(abbreviations);
		
	}

}
