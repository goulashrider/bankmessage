package bankMessageSystem;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class Message implements MessageInit {
	
	
	public String messageHeader;
	public String messageBody;
	public String messageSender;
	public String messageSubject;
	public Integer messageLength;
	
	Message() {
		
	}
	
/*    public void replaceAbbrevation() {

    	
    }*/

	public String getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(String messageHeader) {
		String header = messageHeader.substring(0, 10);
		this.messageHeader = header;
	}

	public String getMessageBody() { 
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	
	public String getMessageSender() {
		return messageSender;
	}

	public void setMessageSender(String messageSender) {
		this.messageSender = messageSender;
	}

	public String getMessageSubject() {
		return messageSubject;
	}

	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}

	public String toString() {
		String string = "Message: \n";
		
		//string += "Type: " + getClass().getCanonicalName() + "\n";
		string += "ID: " + getMessageHeader() + ", ";
		string += "Sender: " + getMessageSender() + ", ";
		string += "Subject: " + getMessageSubject() + ", ";
		string += "Body: " + getMessageBody();

		
		return string;
	}

	
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}
	



}
