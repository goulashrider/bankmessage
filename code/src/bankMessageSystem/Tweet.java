package bankMessageSystem;



import java.util.*;
import java.util.Map.Entry;

/**
 * 
 */
public class Tweet extends Message {

    public Tweet() {
    	this.twitterId = this.getMessageSender();
    	this.messageSubject = null;;
    }

    public String twitterId;

    public ArrayList<Hashtag> hashtags = new ArrayList<>();
    public ArrayList<Mention> mentions = new ArrayList<>();

    public void addHashtagToList(ArrayList<Hashtag> hashes) {
    	if (hashes != null) {
	    	for (Hashtag hash : hashes) {

	    		// count number of given hashtags
	    		if (Messages.getInstance().hashtags.get(hash.toString()) != null) {
	    			// increase counter based on existing number of hashes
		    		Messages.getInstance().hashtags.put(hash.toString(), Messages.getInstance().hashtags.get(hash.toString()) + 1);
	    		} else {
	    			// initialize counter
	    			Messages.getInstance().hashtags.put(hash.toString(), 1);
	    		}
	    		// add hashtags to pane
	    		displayTrendingHashtags();
	    			    		
	    		//test
	    		System.out.println(Messages.getInstance().hashtags.get(hash));
	    		
	    	}
    	}
    }

    public void addIdToMentions(ArrayList<Mention> mentions) {
    	if (mentions != null) {
	    	for (Mention mention : mentions) {
	    		Messages.getInstance().mentions.add(mention);
	    	}
    	}
    }
    
    public void getHashtags() {
    	// load message body into an array
    	// split array by space
    	
    	String bodyArray[] = this.getMessageBody().split(" ");
    	
    	// look for hashtags
    	
    	for (int i=0; i < bodyArray.length; i++) {
    		// look for #
      		
    		if (String.valueOf(bodyArray[i].charAt(0)).equals("#")) {
    			// add to this hashtag attribute
    			Hashtag hashtag = new Hashtag(String.valueOf(bodyArray[i].toString()));
    			this.hashtags.add(hashtag);
    			
    			// displaying hashtags in the main window
    			MainWindow.getInstance().hashItems.addElement(hashtag.toString());
    			//MainWindow.getInstance().listItems.addElement(hashtag.toString());
    			
    		}
    	}
    	
    	// convert body array back to string
    	String joined = String.join(" ", bodyArray);
    	this.setMessageBody(joined);
    	
    	//test
    	
    }
    
    public void getMentions() {
    	// load message body into an array
    	// split array by space
    	
    	String bodyArray[] = this.getMessageBody().split(" ");
    	
    	// look for mentions
    	
    	for (int i=0; i < bodyArray.length; i++) {
    		// look for #
      		
    		if (String.valueOf(bodyArray[i].charAt(0)).equals("@")) {
    			// add to this mention attribute
    			Mention mention = new Mention(String.valueOf(bodyArray[i].toString()));
    			this.mentions.add(mention);
    			
    			// displaying mentions in the main window
    			MainWindow.getInstance().mentionItems.addElement(mention.toString());
    			//MainWindow.getInstance().listItems.addElement(mention.toString());

    		}
    	}
    	
    	// convert body array back to string
    	String joined = String.join(" ", bodyArray);
    	this.setMessageBody(joined);
    	
    	//test
    	
    }
    
    public void abbrevationReplace() {
    	// load message body into an array
    	// split array by space
    	
    	String bodyArray[] = this.messageBody.split(" ");
    	
    	// go through on array and look for abbreviation
    	
    	for (int i=0; i < bodyArray.length; i++) {
    		for(Entry<String, String> entry : Abbreviations.abbreviations.entrySet()){ 
    			
    			// abbrevation checking lower and uppercase as well
    			
    			if ( entry.getKey().toString().equals(bodyArray[i]) || entry.getKey().toString().toLowerCase().equals(bodyArray[i]) ) {
    				
    				// expand text with abbreviation explanation in closing <> 
    				
    				String abbrevExplain = " <" + entry.getValue().toString() + ">";
    				bodyArray[i] = bodyArray[i] + abbrevExplain;
    			}
    		}
    	}
    	
    	// convert body array back to string
    	String joined = String.join(" ", bodyArray);
    	this.setMessageBody(joined);
    	
    	// test
    	//System.out.println(this.getMessageBody());
    }

	@Override
	public void init() {
		this.getHashtags();
		this.getMentions();
		this.abbrevationReplace();
		addHashtagToList(this.hashtags);
		addIdToMentions(this.mentions);

	}
	
	public void displayTrendingHashtags() {
		String string = "";
		for(Entry<String, Integer> hash: Messages.getInstance().hashtags.entrySet()) {
			string += hash.getKey() + ": " + hash.getValue() + "\n";
		}
		// add to display pane
		MainWindow.getInstance().hashTrending.setText(string);
	}

	public String getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}


}