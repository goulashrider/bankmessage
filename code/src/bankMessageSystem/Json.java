package bankMessageSystem;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class Json {
	
	public Json() throws JSONException, IOException {}
	
	static ArrayList<Message> data = new ArrayList();
	   
	public static void save(String json, String fileName) throws IOException {
		
		   try (FileWriter file = new FileWriter("data/" + fileName + ".json")) {
				file.write(json);
				System.out.println("Successfully Copied JSON Object to File...");
				System.out.println("\nJSON Object: " + json);
			}
	}
	
	public static void gsonSave() throws IOException {
		
		String baseJason = "{}";
		JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(baseJason);
		
		Gson gson = new GsonBuilder().create();
		Gson json = new Gson();
		
		JsonElement jsonData = gson.toJsonTree(element);
		JsonObject jsonObject = jsonData.getAsJsonObject();
		
		jsonObject.addProperty("email", Messages.getInstance().emails.toString());
		
/*		
		List<Tweet> tweets = new ArrayList<Tweet>();
		for (Tweet tw: Messages.getInstance().tweets) {
			tweets.add(tw);
		}
		jsonObject.addProperty("tweet", json.toJson(tweets));*/
		
		jsonObject.addProperty("tweet", Messages.getInstance().tweets.toString());
		jsonObject.addProperty("sms", Messages.getInstance().smses.toString()); 
		jsonObject.addProperty("url", Messages.getInstance().urls.toString());
		jsonObject.addProperty("hashtag", Messages.getInstance().hashtags.toString());
		jsonObject.addProperty("mention", Messages.getInstance().mentions.toString());
		jsonObject.addProperty("sir", Messages.getInstance().sirs.toString());
		
		System.out.println("Messages JSON : " + jsonData);
		
		save(jsonData.toString(), "data");
	}

}
