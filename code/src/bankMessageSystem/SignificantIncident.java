package bankMessageSystem;



import java.util.*;

/**
 * 
 */
public class SignificantIncident {

    public SignificantIncident() {
    }

    public String sortCode;

    public String natureOfIncident;

	public String getSortCode() {
		return sortCode;
	}

	public void setSortCode(String sortCode) {
		this.sortCode = sortCode;
	}

	public String getNatureOfIncident() {
		return natureOfIncident;
	}

	public void setNatureOfIncident(String natureOfIncident) {
		this.natureOfIncident = natureOfIncident;
	}
    
    public String toString() {
    	return this.getSortCode() + ", " + this.getNatureOfIncident();
    }


}