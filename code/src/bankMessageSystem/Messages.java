package bankMessageSystem;



import java.util.*;


public class Messages {
	
	private static Messages instance = null;
	
	public ArrayList<Message> messages = new ArrayList<>();
	public ArrayList<Sms> smses = new ArrayList<>();
	public ArrayList<Email> emails = new ArrayList<>();
	public ArrayList<Tweet> tweets = new ArrayList<>();
	public ArrayList<SignificantIncident> sirs = new ArrayList<>();
	
	public ArrayList<Url> urls = new ArrayList<>();
	public ArrayList<Mention> mentions = new ArrayList<>();
	public HashMap<String, Integer> hashtags = new HashMap<String, Integer>();
	
	protected Messages() {
		
	}
	
	public static synchronized Messages getInstance() {
		if (instance == null ) {
			instance = new Messages();
		}
		return instance;
	}
	
	public void addMesssage(ArrayList<Object> arraylist, Object obj ) {
		arraylist.add(obj);
	}
	
	public static String getType(String header) {
		String typeChar = Character.toString(header.charAt(0));
		return typeChar;
	}
	
	public static Message factoryMessage(String type) {
		
		if (type.equals("E") || type.equals("e")) {
			Email obj = new Email();
			Messages.getInstance().emails.add(obj);
			return obj;
		} else if (type.equals("S") || type.equals("s"))  {
			Sms obj = new Sms();
			Messages.getInstance().smses.add(obj);
			return obj;
		} else if (type.equals("T") || type.equals("t")) {
			Tweet obj = new Tweet();
			Messages.getInstance().tweets.add(obj);
			return obj;
		} else {
			return null;
		}
	
	}
	
    
/*    public int countHashtags(ArrayList<Hashtag> hashtags) {
    	for (int numHashes: hashtags) {
    		
    	}
    
    	return null;
    }
    */
	
    public void save() {
        // TODO implement here
    }

    public void load() {
        // TODO implement here
    }

	public ArrayList<Tweet> getTweets() {
		return tweets;
	}

	public void setTweets(ArrayList<Tweet> tweets) {
		this.tweets = tweets;
	}
    
    


}