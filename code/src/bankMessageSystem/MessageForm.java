package bankMessageSystem;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFormattedTextField;
import javax.swing.JEditorPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MessageForm extends JFrame {

	private JPanel contentPane;
	public JTextField header;
	private JButton btnSend;
	public JTextField sender;
	private JLabel lblSender;
	public JTextField subject;
	private JLabel lblSubject;
	public JEditorPane body;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MessageForm frame = new MessageForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public MessageForm() {
		init();
		this.setVisible(true);
	}
	
	public void init() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 457, 572);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHeader = new JLabel("Header");
		lblHeader.setBounds(10, 11, 46, 14);
		contentPane.add(lblHeader);
		
		header = new JTextField();
		header.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent ke) {
				keyCheck(ke.getKeyChar());
			}
		});
		header.setBounds(59, 8, 323, 20);
		contentPane.add(header);
		header.setColumns(10);
		//header.setEditable(false);
		
		JLabel lblBody = new JLabel("Body");
		lblBody.setBounds(10, 251, 46, 14);
		contentPane.add(lblBody);
		
		btnSend = new JButton("Send");
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendAction();
				
			}
		});
		btnSend.setBounds(173, 499, 89, 23);
		contentPane.add(btnSend);
		
		sender = new JTextField();
		sender.setBounds(58, 44, 324, 20);
		contentPane.add(sender);
		sender.setColumns(10);
		
		lblSender = new JLabel("Sender");
		lblSender.setBounds(10, 47, 46, 14);
		contentPane.add(lblSender);
		
		subject = new JTextField();
		subject.setBounds(59, 75, 323, 20);
		contentPane.add(subject);
		subject.setColumns(10);
		
		lblSubject = new JLabel("Subject");
		lblSubject.setBounds(10, 78, 46, 14);
		contentPane.add(lblSubject);
		
		body = new JEditorPane();
		body.setBounds(59, 127, 323, 346);
		contentPane.add(body);
	}
	
	public void sendAction() {
		Message message;
		
		// get form field values
		String headerText = getHeader().getText();
		String bodyText = getBody().getText();
		
		// call factory function to create Message object
		// no validation implemented for wrong type of ID
		message = Messages.factoryMessage(Messages.getType(headerText));
		
		//Messages.getInstance().fieldLogic(headerText, this);
		//create random ID number
		int randomNum = 111111111 + (int)(Math.random() * 999999999); 
		
		//convert head into string
		String head = headerText.charAt(0) + Integer.toString(randomNum);
		
		//set object parameters
		message.setMessageHeader(head.toUpperCase());
		message.setMessageBody(bodyText);
		message.setMessageSender(sender.getText());
		message.setMessageSubject(subject.getText());
		
		
		// Initialising Message type field with init() method
		message.init();
		//message.replaceAbbrevation();
		System.out.println(message);
		
		//adding message to messages list
		Messages.getInstance().messages.add(message);
		
		//close submission window
		dispose();
		
		//System.out.println(Messages.getInstance().messages);
		
		// displaying message in the main window
		MainWindow.getInstance().listItems.addElement(message.toString());
		

	}
	
	public void keyCheck(char ke) {
		//Messages.getInstance().fieldLogic(ke, this);
	}
	

	public JEditorPane getBody() {
		return body;
	}

	public void setBody(JEditorPane body) {
		this.body = body;
	}

	public JTextField getHeader() {
		return header;
	}

	public void setHeader(JTextField header) {
		this.header = header;
	}
}
