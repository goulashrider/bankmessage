package bankMessageSystem;

public class Url {
	
	public String url;
	
	Url(String name) {
		this.url = name;
	}

	public String getName() {
		return url;
	}

	public void setName(String name) {
		this.url = name;
	}
	
	public String toString() {
		return this.url;
	}
	

}
