package bankMessageSystem;

import java.util.ArrayList;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;

public class Controller {
	
	private static Controller instance; 
	
	public Messages messages = Messages.getInstance();
	public ArrayList<JFrame> frames;
	public static MainWindow mainFrame;
	

	protected Controller() {
		mainFrame = MainWindow.getInstance();
		mainFrame.show();
		Abbreviations.getInstance();

	}
	
	public void addMessage(Message message) {
		messages.messages.add(message);
	}

	public static synchronized Controller getInstance() {
		if (instance == null) {
			instance = new Controller();
		}
		return instance;
	}
	



}
