package bankMessageSystem;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * 
 */
public class Email extends Message {

    public String url;
    public static String[] natureOfIncidents = new String[20];
    
    public Email() {
    	natureOfIncidents[0] = "Theft";
    	natureOfIncidents[1] = "Staff Attack";
    	natureOfIncidents[2] = "ATM Theft";
    	natureOfIncidents[3] = "Raid";
    	natureOfIncidents[4] = "Customer Attack";
    	natureOfIncidents[5] = "Staff Abuse";
    	natureOfIncidents[6] = "Bomb Threat";
    	natureOfIncidents[7] = "Terrorism";
    	natureOfIncidents[8] = "Suspicious Incident";
    	natureOfIncidents[9] = "Intelligence";
    	natureOfIncidents[10] = "Cash Loss";
    }
    
	@Override
	public void init() {
		//before body search subject extraction must be done to not lose \n
		sirIdentfy();
		//urlReplace();
	}
	
	public void urlReplace() {
    	// load message body into an array
    	// split array by space
    	
    	String bodyArray[] = this.messageBody.split(" ");
    	
    	// regular expression for web url
    	String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    	
    	Pattern pattern = Pattern.compile(regex);
    	
    	// go through on array and look for url
    	
    	for (int i=0; i < bodyArray.length; i++) {
    		Matcher matcher = pattern.matcher(bodyArray[i].toString());
    			if ( matcher.matches()  ) {
    				
    				// add URL to quarantined list
    				Messages.getInstance().urls.add(new Url(bodyArray[i]));
    				
    				//display quarantined URL
    				displayUrl(bodyArray[i]);
    				
    				// replace URL  with <....>
    				String urlMasked = "<URL Quarantined>";
    				bodyArray[i] = urlMasked;
    			}
    	}
    	
    	// convert body array back to string
    	String joined = String.join(" ", bodyArray);
    	this.setMessageBody(joined);
    	
    	// test
    	//System.out.println(this.getMessageBody());
    }
	
	public void sirIdentfy() {
		// read body lines
		String[] lines = this.messageBody.split(System.getProperty("line.separator"));
		
		// do URL masking
		for (int i=0; i < lines.length; i++) {
			urlReplace();
		}
		// read subject
		// if there is a sir pattern - �SIR dd/mm/yy� -> make regex
		String regex = "SIR [0-9][0-9]/[0-9][0-9]/[0-9][0-9]";
    	Pattern pattern = Pattern.compile(regex);
    	Matcher matcher = pattern.matcher(this.messageSubject);
    	
		if (matcher.matches()) {
			System.out.println("Email subject is valid for SIR!");
			// read first two lines of body - Sort Code: 99-99-99, Nature of Incident -> list comes form attribute Array

			
			// check if there is more than two lines
			if (lines.length >= 2) {
				SignificantIncident sir = new SignificantIncident();
				sir.setSortCode(lines[0]);
				sir.setNatureOfIncident(lines[1]);
								
				// match Nature of Incident
				String regexNature = "Nature of Incident: $";
				Pattern patternNature = Pattern.compile(regexNature);
				Matcher matchNature = patternNature.matcher(lines[0]);
				
				if (matchNature.matches()) {
					System.out.println("Nature of incident valid!");
					
					for(String nature: this.natureOfIncidents) {
						if (lines[0].substring(19, lines[0].length()).toUpperCase().equals(nature.toUpperCase())) {
							System.out.println("Nature of incident matches with list");
						} else {
							System.out.println("Nature of incident doesn't match with list!");
						}
					}
				} else {
					System.out.println("Email body format is incorrect for SIR!");
				}
				
				// write SIR to SIR list and display in main window
				Messages.getInstance().sirs.add(sir);
			}
			System.out.println(Messages.getInstance().sirs.toString());
		}
		
		
	}
	
	public void displayUrl(String url) {
		MainWindow.getInstance().urlItems.addElement(url);
	}


}