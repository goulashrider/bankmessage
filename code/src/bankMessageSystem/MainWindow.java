package bankMessageSystem;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;

import sun.security.jca.GetInstance.Instance;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

public class MainWindow {
	
	protected static MainWindow instance;

	public JFrame frame;
	private JList list;
	private JButton btnSendMessage;
	private JList mentions;
	private JList hashtags;
	public JTextPane hashTrending;
	private JLabel lblUrls;
	private JList urls;

	public DefaultListModel listItems = new DefaultListModel<>();
	public DefaultListModel mentionItems = new DefaultListModel<>();
	public DefaultListModel hashItems = new DefaultListModel<>();
	public DefaultListModel urlItems = new DefaultListModel<>();
	private JButton btnSaveData;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the application.
	 */
	private MainWindow() {
		initialize();
		//this.frame.setVisible(true);
	}
	
	public static synchronized MainWindow getInstance() {
		if ( instance == null ) {
			instance = new MainWindow();
		}
		return instance;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 650, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		btnSendMessage = new JButton("New message");
		btnSendMessage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new MessageForm();
			}
		});
		btnSendMessage.setBounds(10, 11, 127, 23);
		frame.getContentPane().add(btnSendMessage);
		 
		JLabel lblMessages = new JLabel("Messages");
		lblMessages.setBounds(271, 15, 84, 14);
		frame.getContentPane().add(lblMessages);
		
		list = new JList(listItems);
		list.setBounds(20, 52, 589, 189);
		frame.getContentPane().add(list);
		
		JLabel lblMentions = new JLabel("Mentions:");
		lblMentions.setBounds(20, 264, 84, 14);
		frame.getContentPane().add(lblMentions);
		
		JLabel lblTrendingHashtags = new JLabel("Trending hashtags:");
		lblTrendingHashtags.setBounds(140, 264, 172, 14);
		frame.getContentPane().add(lblTrendingHashtags);
		
		mentions = new JList(mentionItems);
		mentions.setBounds(20, 289, 95, 140);
		frame.getContentPane().add(mentions);
				
		hashtags = new JList(hashItems);
		hashtags.setBounds(140, 289, 71, 140);
		frame.getContentPane().add(hashtags);
		
		hashTrending = new JTextPane();
		hashTrending.setBounds(220, 289, 127, 140);
		frame.getContentPane().add(hashTrending);
		
		urls = new JList(urlItems);
		urls.setBounds(357, 289, 216, 140);
		frame.getContentPane().add(urls);
		
		lblUrls = new JLabel("URLs:");
		lblUrls.setBounds(357, 264, 46, 14);
		frame.getContentPane().add(lblUrls);
		
		btnSaveData = new JButton("Save data");
		btnSaveData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Json.gsonSave();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnSaveData.setBounds(474, 11, 135, 23);
		frame.getContentPane().add(btnSaveData);
	}
	
	public void show() {
		this.frame.setVisible(true);
	}
	
	public void hide() {
		this.frame.setVisible(false);
	}
}
